import { UserModel } from '@/models/user'
import getInitials from './getInitials'
const avatar = (user: UserModel['credentials']): string => {
	if (user.avatar) {
		return process.env.VUE_APP_IMAGES_FOLDER + '/users/' + user.avatar
	} else {
		return getInitials(user.name)
	}
}
export default avatar
