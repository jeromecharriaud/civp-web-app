import { MutationTree } from 'vuex'
import { ProductStore, ProductModel } from '@/models/product'

const getDefaultState = () => {
	return {
		products: [] as ProductModel[],
		liked: [] as ProductModel[],
	}
}

export const mutations: MutationTree<ProductStore> = {
	SET_PRODUCTS(state, payload: ProductModel[]) {
		state.products = state.products.concat(payload)
	},
	SET_LIKED_PRODUCTS(state, payload: ProductModel[]) {
		state.liked = state.liked.concat(payload)
	},

	ADD_PRODUCT(state, payload: ProductModel) {
		state.products = state.products.concat(payload)
	},
	EDIT_LIKE_PRODUCT(state, payload: ProductModel) {
		const index = state.products.findIndex(
			(product) => product.id == payload.id
		)
		state.products[index].liked = payload.liked
	},
	EDIT_PRODUCT(state, payload: ProductModel) {
		const index = state.products.findIndex(
			(product) => product.id == payload.id
		)
		state.products[index] = payload
	},
	DELETE_PRODUCT(state, payload: ProductModel) {
		state.products.filter((p) => {
			p.id != payload.id
		})
	},

	RESET_STATE(state) {
		Object.assign(state, getDefaultState())
	},
}
