import { Module } from 'vuex'
import { ProductStore } from '@/models/product'
import { RootState } from '@/models/store'
import { getters } from '@/store/product/getters'
import { mutations } from '@/store/product/mutations'
import { actions } from '@/store/product/actions'

const state: ProductStore = {
	products: [],
	liked: [],
}

export const product: Module<ProductStore, RootState> = {
	state,
	getters,
	mutations,
	actions,
}
