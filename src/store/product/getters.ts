import { GetterTree } from 'vuex'
import { ProductStore, ProductModel } from '@/models/product'
import { RootState } from '@/models/store'

export const getters: GetterTree<ProductStore, RootState> = {
	getProducts(state): ProductModel[] {
		return state.products
	},
	getLikedProducts(state): ProductModel[] {
		return state.liked
	},
	// getDislikedProducts(state): ProductModel[] {
	// 	return state.disliked
	// },
}
