/* eslint-disable no-empty-pattern */
import { RootState } from '@/models/store'
import { ProductModel, ProductStore } from '@/models/product'
import { ActionTree } from 'vuex'

//API
import productApi from '@/api/product.api'
import LikesApi from '@/api/like.api'
import { LikeModel } from '@/models/like'

export const actions: ActionTree<ProductStore, RootState> = {
	async fetchProducts({ commit }): Promise<void> {
		const [error, response] = await productApi.get()
		if (response && response.result && !error) {
			const products = response.result
			commit('SET_PRODUCTS', products)
		} else if (error) {
			throw new Error(error.message)
		}
	},
	async fetchLikedProducts({ commit, getters }): Promise<void> {
		const [error, response] = await LikesApi.get({
			by: 'user',
			liked: true,
		})
		if (response && response.result && !error) {
			const likedProducts: LikeModel[] = response.result
			const products: ProductModel[] = getters.getProducts
			const allProducts: ProductModel[] = products.map((p) => {
				const found = likedProducts.find((likedProduct) => {
					return (
						p.id == likedProduct.product_id &&
						likedProduct.liked === true
					)
				})
				if (found) {
					p.liked = true
				} else {
					p.liked = false
				}
				return p
			})
			commit('SET_LIKED_PRODUCTS', allProducts)
		} else if (error) {
			console.log(error)
			throw new Error(error.message)
		}
	},
	async likeProduct(
		{ commit, getters },
		payload: {
			user_id: string
			product: ProductModel
		}
	): Promise<void> {
		const product_id = payload.product.id
		const [error, data] = await LikesApi.like({
			user_id: payload.user_id,
			product_id: product_id,
		})
		if (data && !error) {
			const product: ProductModel = getters.getLikedProducts.find(
				(p: ProductModel) => p.id == product_id
			)
			if (product !== undefined) {
				commit('EDIT_PRODUCT', product)
			}
		}
	},
}
