import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'
import { RootState } from '@/models/store'
import { user } from '@/store/user'
import { app } from '@/store/app'
import { product } from '@/store/product'

Vue.use(Vuex)

const store: StoreOptions<RootState> = {
	state: {},
	mutations: {},
	actions: {},
	modules: {
		user,
		app,
		product,
	},
}

export default new Vuex.Store<RootState>(store)
