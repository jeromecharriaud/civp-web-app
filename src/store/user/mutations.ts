import Vue from 'vue'
import { UserModel } from '@/models/user'
import { MutationTree } from 'vuex'

const getDefaultState = () => {
	return {
		loggedIn: false,
		credentials: [],
		capabilities: [],
		productsApiPage: 1,
		productsIndex: 1,
		token: '',
	}
}

export const mutations: MutationTree<UserModel> = {
	SET_USER_CREDENTIALS(state, payload: any) {
		Vue.set(state, 'credentials', payload)
	},	
    SET_USER_CAPABILITIES(state, payload: any) {
		Vue.set(state, 'capabilities', payload)
	},
	SET_USER_LOGGED_STATUS(state, payload: boolean) {
		state.loggedIn = payload
	},
	SET_USER_PRODUCTS_API_PAGE(state, payload: number) {
		state.credentials.products_api_page = payload
	},
	SET_USER_PRODUCTS_API_INDEX(state, payload: number) {
		state.credentials.products_api_index = payload
	},
	SET_USER_TOKEN(state, payload: string) {
		state.token = payload
	},
	RESET_STATE(state) {
		Object.assign(state, getDefaultState())
	},
}
