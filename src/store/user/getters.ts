import { GetterTree } from 'vuex'
import { UserModel } from '@/models/user'
import { RootState } from '@/models/store'

export const getters: GetterTree<UserModel, RootState> = {
	getUserInfo(state): any {
		return state.credentials
	},
	getUserCapabilities(state): any {
		return state.capabilities
	},
	getToken(state): string {
		return state.token
	},
	getUserLoggedStatus(state): boolean {
		return state.loggedIn
	},
}
