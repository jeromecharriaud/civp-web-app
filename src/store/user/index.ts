import { Module } from 'vuex'
import { UserModel } from '@/models/user'
import { RootState } from '@/models/store'
import { getters } from '@/store/user/getters'
import { mutations } from '@/store/user/mutations'
import { actions } from '@/store/user/actions'

const state = {} as UserModel

export const user: Module<UserModel, RootState> = {
	state,
	getters,
	mutations,
	actions,
}
