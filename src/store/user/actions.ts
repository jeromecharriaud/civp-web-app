/* eslint-disable no-empty-pattern */
import { RootState } from '@/models/store'
import { UserModel } from '@/models/user'
import { ActionTree } from 'vuex'
import router from '@/_app/router'

//API
import UsersApi from '@/api/user.api'

export const actions: ActionTree<UserModel, RootState> = {
	async loginUser({ dispatch, getters }, payload): Promise<void> {
		const [error, data] = await UsersApi.login(payload)
		if (data) {
			const token = data.token
			const credentials = data.credentials

			//Account activated ?
			if (!credentials.activated) {
				throw new Error(
					'Your account is not activated. Please check your emails or contact the administrator.'
				)
			}

			//Set Credentials
			localStorage.setItem('token', token)

			//Launch the App again
			await dispatch('launchApp', {
				type: getters.getEnvironmentType,
			})

			if (router.currentRoute.query.redirect === 'admin') {
				window.location.href = process.env.VUE_APP_ADMIN_URL
			} else {
				router.push('/')
			}
		} else {
			throw new Error(error.message)
		}
	},
	logoutUser({ commit, getters }) {
		const type = getters.getAppType
		commit('RESET_STATE')
		localStorage.removeItem('token')
		if (type === 'app') {
			router.push({ name: 'login' })
		} else {
			window.location.href = process.env.VUE_APP_BASE_URL
		}
	},

	async setUserCredentials({ commit }, payload): Promise<void> {
		const [error, response] = await UsersApi.getSingle({
			user_id: payload.user_id,
		})
		if (response && response.credentials) {
			commit('SET_USER_CREDENTIALS', response.credentials)
			commit('SET_USER_CAPABILITIES', response.capabilities)
			commit('SET_USER_LOGGED_STATUS', true)
			commit('SET_USER_TOKEN', payload.token)
		} else {
			throw new Error(error.message)
		}
	},

	async updateUser({ commit }, payload) {
		const [error, response] = await UsersApi.edit(payload)
		if (response) {
			commit('SET_USER_CREDENTIALS', response.credentials)
			return response.credentials
		} else {
			throw new Error(error?.message)
		}
	},
	async registerUser({}, payload): Promise<UserModel> {
		const [error, response] = await UsersApi.add(payload)
		if (!error && response) {
			return response
		} else {
			if (error.code == 'auth/email-already-in-use') {
				throw new Error(
					'This email is already used. Please choose another one.'
				)
			} else {
				throw new Error(error?.message)
			}
		}
	},
}
