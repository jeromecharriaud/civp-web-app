import { GetterTree } from 'vuex'
import AppModel from '@/models/app'
import { RootState } from '@/models/store'

export const getters: GetterTree<AppModel, RootState> = {
	getEnvironmentType(state): string {
		return state.type
	},
}
