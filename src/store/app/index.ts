import { Module } from 'vuex'
import AppModel from '@/models/app'
import { RootState } from '@/models/store'
import { getters } from '@/store/app/getters'
import { mutations } from '@/store/app/mutations'
import { actions } from '@/store/app/actions'

const state = {} as AppModel

export const app: Module<AppModel, RootState> = {
	state,
	getters,
	mutations,
	actions,
}
