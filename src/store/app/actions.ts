/* eslint-disable no-empty-pattern */
import { RootState } from '@/models/store'
import AppModel from '@/models/app'
import { ActionTree } from 'vuex'

const jwt = require('jsonwebtoken')

export const actions: ActionTree<AppModel, RootState> = {
	async launchApp({ commit, dispatch, getters }, payload): Promise<void> {
		commit('SET_ENVIRONMENT_TYPE', payload.type)
		const token = localStorage.getItem('token')
		if (token) {
			//Decode Token
			try {
				//Init User Credentials
				const decoded = await jwt.verify(
					localStorage.getItem('token'),
					process.env.VUE_APP_TOKEN_KEY
				)
				const user_id = decoded._id

				//Get User Credentials
				await dispatch('setUserCredentials', {
					user_id: user_id,
					token: token,
				})
				//Get Products in case tere are none
				const products = getters.getProducts
				if (products.length === 0) {
					await dispatch('fetchProducts')
				}

				//Get Products Liked
				const likedProducts = getters.getLikedProducts
				if (likedProducts.length === 0) {
					await dispatch('fetchLikedProducts', { user_id: user_id })
				}
			} catch (e) {
				//Invalid Token or Token key : logout user
				// dispatch('logoutUser')
			}
		}
	},
}
