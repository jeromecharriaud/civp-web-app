import AppModel from '@/models/app'
import { MutationTree } from 'vuex'

const getDefaultState = () => {
	return {
		type: 'app',
	}
}

export const mutations: MutationTree<AppModel> = {
	SET_ENVIRONMENT_TYPE(state, payload: string) {
		state.type = payload
	},
	RESET_STATE(state) {
		Object.assign(state, getDefaultState())
	},
}
