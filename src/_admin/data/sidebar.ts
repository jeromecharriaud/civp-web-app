const links = [
	{
		id: 'listProducts',
		icon: 'mdi-view-list-outline',
		title: 'Products',
		link: '/',
	},
	{
		id: 'listEvents',
		icon: 'mdi-calendar-outline',
		title: 'Events',
		link: '/events',
	},
	{
		id: 'listUsers',
		icon: 'mdi-text-account',
		title: 'Users',
		link: '/users',
	},
]

export default links
