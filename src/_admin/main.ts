import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from '@/store'
import vuetify from '@/plugins/vuetify'
import axiosInterceptor from '@/axios'

Vue.config.productionTip = false

axiosInterceptor()
store.dispatch('launchApp', { type: 'admin' }).then(() => {
	new Vue({
		router,
		store,
		vuetify,
		render: (h) => h(App),
	}).$mount('#app')
})
