import { RouteConfig } from 'vue-router'
const routesClient: Array<RouteConfig> = [
	{
		path: '*',
		component: () => import('/src/_app/views/404.vue'),
		name: '404',
		meta: { requiresAuth: false },
	},
	{
		path: '/',
		component: () => import('../views/products/productAdmin.vue'),
		name: 'home',
	},
	{
		path: '/login',
		component: () => import('/src/_app/views/auth/login.vue'),
		name: 'login',
	},

	{
		path: '/events',
		component: () => import('../views/events/eventAdmin.vue'),
		name: 'events',
	},
	{
		path: '/events/s/:eventId',
		component: () => import('/src/_app/views/events/singleEvent.vue'),
		name: 'singleEvent',
		meta: { requiresAuth: true },
	},
	{
		path: '/events/edit/:eventId',
		component: () => import('../views/events/eventEdit.vue'),
		name: 'editEvent',
	},
	{
		path: '/events/add',
		component: () => import('../views/events/eventAdd.vue'),
		name: 'addEvent',
	},

	{
		path: '/products',
		component: () => import('../views/products/productAdmin.vue'),
		name: 'products',
	},
	{
		path: '/products/s/:productId',
		component: () => import('/src/_app/views/products/singleProduct.vue'),
		name: 'singleProduct',
		meta: { requiresAuth: true },
	},
	{
		path: '/products/edit/:productId',
		component: () => import('../views/products/productEdit.vue'),
		name: 'editProduct',
	},
	{
		path: '/products/add',
		component: () => import('../views/products/productAdd.vue'),
		name: 'addProduct',
	},
	{
		path: '/users',
		component: () => import('../views/users/userAdmin.vue'),
		name: 'users',
	},
	{
		path: '/users/edit/:user_id',
		component: () => import('../views/users/userEdit.vue'),
		name: 'editUser',
	},
	{
		path: '/users/add',
		component: () => import('../views/users/userAdd.vue'),
		name: 'addUser',
	},
]
export default routesClient
