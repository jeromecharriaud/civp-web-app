import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import store from '@/store'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL + 'admin/',
	routes,
})

router.beforeEach((to: any, from, next) => {
	const isUserLoggedIn = store.getters.getUserLoggedStatus
	const userInfo = store.getters.getUserInfo
	let userRole
	if (userInfo) {
		userRole = userInfo.role
	}
	switch (true) {
		//Redirect non admin users
		case !isUserLoggedIn || (userRole != 'administrator') === true:
			window.location.href =
				process.env.VUE_APP_BASE_URL + '/login?redirect=admin'
			break
		default:
			next()
	}
})

export default router
