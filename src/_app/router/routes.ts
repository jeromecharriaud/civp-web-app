import { RouteConfig } from 'vue-router'
const routesClient: Array<RouteConfig> = [
	{
		path: '*',
		component: () => import('../views/404.vue'),
		name: '404',
		meta: { requiresAuth: false },
	},
	{
		path: '/',
		component: () => import('../views/home.vue'),
		name: 'home',
		meta: { requiresAuth: true },
	},
	{
		path: '/calendar',
		component: () => import('../views/events/viewEvents.vue'),
		name: 'calendar',
		meta: { requiresAuth: true },
	},
	{
		path: '/profile',
		component: () => import('../views/users/profileUser.vue'),
		name: 'profile',
		meta: { requiresAuth: true },
	},
	{
		path: '/events/s/:eventId',
		component: () => import('../views/events/singleEvent.vue'),
		name: 'singleEvent',
		meta: { requiresAuth: true },
	},
	{
		path: '/login',
		component: () => import('../views/auth/login.vue'),
		name: 'login',
		meta: { requiresAuth: false },
	},
	{
		path: '/register',
		component: () => import('../views/auth/register.vue'),
		name: 'register',
		meta: { requiresAuth: false },
	},
	{
		path: '/lost-password',
		component: () => import('../views/auth/lostPassword.vue'),
		name: 'lostPassword',
		meta: { requiresAuth: false },
	},
	{
		path: '/activate',
		component: () => import('../views/auth/activate.vue'),
		name: 'activateUser',
		meta: { requiresAuth: false },
	},
	{
		path: '/products',
		component: () => import('../views/products/listProducts.vue'),
		name: 'products',
		meta: { requiresAuth: true },
	},
	{
		path: '/products/add',
		component: () => import('/src/_admin/views/products/productAdd.vue'),
		name: 'addProduct',
	},
	{
		path: '/products/s/:productId',
		component: () => import('../views/products/singleProduct.vue'),
		name: 'singleProduct',
		meta: { requiresAuth: true },
	},
	{
		path: '/likes',
		component: () => import('../views/likes/viewLikes.vue'),
		name: 'myLikes',
		meta: { requiresAuth: true },
	},
	{
		path: '/directory',
		component: () => import('../views/users/listUsers.vue'),
		name: 'users',
	},
	{
		path: '/users/u/:username',
		component: () => import('../views/users/singleUser.vue'),
		name: 'user',
	},
]
export default routesClient
