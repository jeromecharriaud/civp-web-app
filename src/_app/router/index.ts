import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import routes from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
})

router.beforeEach((to: any, from, next) => {
	const isUserLoggedIn = store.getters.getUserLoggedStatus
	const userInfo = store.getters.getUserInfo
	let userRole
	if (userInfo) {
		userRole = userInfo.role
	}
	switch (true) {
		//Not Logged In and Not Public Page
		case !isUserLoggedIn && to.meta.requiresAuth === true:
			next('/login')
			break
		//Logged in and does not required an auth
		case isUserLoggedIn && to.meta.requiresAuth === false:
			next('/')
			break
		//Logged in and requires a specific role
		case isUserLoggedIn &&
			to.meta.requiresAuth === true &&
			to.meta.role &&
			!to.meta.role.includes(userRole):
			next('/')
			break
		default:
			next()
	}
})
export default router
