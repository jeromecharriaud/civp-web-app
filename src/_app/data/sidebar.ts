const links = [
	{
		id: 'home',
		link: '/',
		icon: 'mdi-home',
		title: 'Home',
	},
	{
		id: 'listUsers',
		icon: 'mdi-text-account',
		title: 'Directory',
		link: '/directory',
	},
	{
		id: 'calendar',
		link: '/calendar',
		icon: 'mdi-calendar',
		title: 'Calendar',
	},
	{
		id: 'products',
		link: '/products',
		icon: 'mdi-bottle-wine',
		title: 'Products',
	},
	{
		id: 'likes',
		link: '/likes',
		icon: 'mdi-account-heart',
		title: 'Likes',
	},
]

export default links
