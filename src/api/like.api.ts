//Axios
import axios, { AxiosResponse } from 'axios'
//Models
import { LikeApiResponse, LikeModel } from '@/models/like'
import apiResponse from '@/models/apiResponse'
import ErrorModel from '@/models/errorModel'

export default {
	//Add Like
	async like(
		formData: LikeModel
	): Promise<[ErrorModel | null, LikeApiResponse | null]> {
		try {
			const response: AxiosResponse = await axios.post<apiResponse>(
				process.env.VUE_APP_API_URL + '/likes/add',
				formData
			)
			const data = <LikeApiResponse>response.data
			return [null, data]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Get Like
	async get(params: {
		by?: string
		liked?: boolean
		proudct_id?: number
	}): Promise<[ErrorModel | null, LikeApiResponse | null]> {
		try {
			const response: AxiosResponse = await axios.get<apiResponse>(
				process.env.VUE_APP_API_URL + '/likes',
				{ params }
			)
			const data = <LikeApiResponse>response.data
			return [null, data]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
}
