//Axios
import axios, { AxiosResponse } from 'axios'
//Models
import {
	EventsApiResponse,
	EventApiResponse,
	EventForm,
	EventModel,
} from '@/models/event'
import apiResponse from '@/models/apiResponse'
import errorModel from '@/models/errorModel'

import ErrorModel from '@/models/errorModel'

export default {
	//Get Events
	async get(params?: {
		page?: number
		events?: number[]
	}): Promise<[errorModel | null, apiResponse | null]> {
		try {
			const response: AxiosResponse = await axios.get<apiResponse>(
				process.env.VUE_APP_API_URL + '/events/',
				{ params }
			)

			const data = <EventsApiResponse>response.data
			const events: EventModel[] = data.result
			if (!events) {
				throw new Error('No events Fetched')
			}
			//Transform
			events.map((e: any) => {
				e.title = e.title.replace(/\\/g, '')
			})
			return [null, data]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Get Single Event
	async getSingle(
		eventId: number
	): Promise<[errorModel | null, EventModel | null]> {
		try {
			const response: AxiosResponse = await axios.get<EventModel>(
				process.env.VUE_APP_API_URL + '/events/event/' + eventId
			)
			const event = <EventModel>response.data
			if (!event) {
				throw new Error('No Event to show !')
			}
			//Transform
			event.title = event.title.replace(/\\/g, '')
			return [null, event]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Add Event
	async add(
		formData: EventForm
	): Promise<[errorModel | null, EventModel | null]> {
		try {
			const response: AxiosResponse = await axios.post(
				process.env.VUE_APP_API_URL + '/events/add',
				formData
			)
			const data = <EventApiResponse>response.data
			const newEvent: EventModel = data.result

			return [null, newEvent]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Edit Event
	async edit(
		formData: EventForm
	): Promise<[errorModel | null, EventModel | null]> {
		try {
			const response: AxiosResponse = await axios.put(
				process.env.VUE_APP_API_URL + '/events/' + formData.id,
				formData
			)
			const editedEvent = <EventModel>response.data
			return [null, editedEvent]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Register Event
	async register(params: {
		event_id: number
		user_id: string
	}): Promise<[errorModel | null, EventModel | null]> {
		try {
			const response: AxiosResponse = await axios.put(
				process.env.VUE_APP_API_URL +
					'/events/' +
					params.event_id +
					'/register',
				{ user_id: params.user_id }
			)
			const event = <EventModel>response.data
			return [null, event]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Delete Event
	async delete(
		eventId: number
	): Promise<[errorModel | null, EventModel | any]> {
		try {
			const response: AxiosResponse = await axios.delete<EventModel>(
				process.env.VUE_APP_API_URL + '/events/' + eventId
			)
			const data = <EventModel>response.data

			if (data) {
				return [null, data]
			} else {
				throw new Error("Can't delete the event !")
			}
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
}
