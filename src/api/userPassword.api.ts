//Axios
import axios, { AxiosResponse } from 'axios'
//Models
import ErrorModel from '@/models/ErrorModel'
import { UserModel } from '@/models/user'
import errorModel from '@/models/ErrorModel'

export default {
	//Get Password
	async get(user_id: string): Promise<[errorModel | null, string | any]> {
		try {
			const password: string = await axios.get(
				process.env.VUE_APP_API_URL + '/users/' + user_id + '/password'
			)
			return [null, password]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},

	//Get Lost Password Token
	async getToken(
		email: string
	): Promise<[errorModel | null, UserModel | null]> {
		try {
			const response: AxiosResponse = await axios.post<UserModel>(
				process.env.VUE_APP_API_URL + '/users/lost-password',
				{ email: email }
			)
			const data: any = response.data
			if (!data.token) {
				throw new Error('No Token Fetched!')
			}
			return [null, data]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},

	//Update Password
	async update(
		formData: {
			email?: string | boolean
			password: string | boolean
		},
		token?: string
	): Promise<[errorModel | null, UserModel | any]> {
		try {
			const url = process.env.VUE_APP_API_URL + '/users/update-password'
			let response: AxiosResponse
			if (token) {
				response = await axios.put<UserModel>(url, formData, {
					headers: { Authorization: `Bearer ${token}` },
				})
			} else {
				response = await axios.put<UserModel>(url, formData)
			}

			const data: any = response.data

			return [null, data]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},

	//Check Password
	async checkToken(
		token: string
	): Promise<[errorModel | null, boolean | any]> {
		try {
			const result: AxiosResponse = await axios.post<any>(
				process.env.VUE_APP_API_URL +
					'/users/lost-password-check-token',
				{ token: token }
			)
			const data: any = result.data
			return [null, data]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
}
