//Axios
import axios, { AxiosResponse } from 'axios'
//Models
import ErrorModel from '@/models/errorModel'
import EmailModel from '@/models/email'

export default {
	//Send Email
	async send(
		body: EmailModel
	): Promise<[ErrorModel | null, { token: string } | null]> {
		try {
			const response: AxiosResponse = await axios.post(
				process.env.VUE_APP_API_URL + '/email/send',
				body
			)
			const data: any = response.data
			return [null, data]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
}
