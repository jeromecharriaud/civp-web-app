// import http from '../http-common'
import ErrorModel from '@/models/errorModel'
import errorModel from '@/models/errorModel'
import axios, { AxiosResponse } from 'axios'
const FormData = require('form-data')

interface FileInterface {
	message: string
	name: string
	path: string
}

class UploadFilesService {
	async upload(
		file: File,
		route: string
	): Promise<[errorModel | null, FileInterface | null]> {
		try {
			const formData = new FormData()
			formData.append('file', file)
			formData.append('route', route)
			const response: AxiosResponse = await axios.post(
				process.env.VUE_APP_API_URL + '/upload',
				formData,
				{
					headers: {
						'Content-Type': 'multipart/form-data',
					},
				}
			)
			const dataFile: any = response.data
			return [null, dataFile]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	}
	async delete(
		name: string,
		route: string
	): Promise<[errorModel | null, FileInterface | null]> {
		try {
			const response: AxiosResponse = await axios.delete(
				process.env.VUE_APP_API_URL + '/upload',
				{
					data: {
						name: name,
						route: route,
					},
				}
			)
			const dataFile: any = response.data
			return [null, dataFile]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	}
}

export default new UploadFilesService()
