//Axios
import axios, { AxiosResponse } from 'axios'
//Models
import {
	ProductsApiResponse,
	ProductApiResponse,
	ProductForm,
	ProductModel,
} from '@/models/product'
import apiResponse from '@/models/apiResponse'
import errorModel from '@/models/errorModel'
import fileApi from './file.api'

//Import Store
import store from '@/store'
import ErrorModel from '@/models/errorModel'

export default {
	//Get Products
	async get(params?: {
		user_id?: string
		page?: number
		products?: number[]
	}): Promise<[errorModel | null, apiResponse | null]> {
		try {
			const response: AxiosResponse = await axios.get<apiResponse>(
				process.env.VUE_APP_API_URL + '/products/',
				{ params }
			)

			const data = <ProductsApiResponse>response.data
			const products: ProductModel[] = data.result

			if (!products) {
				throw new Error('No products Fetched')
			}
			//Transform
			products.map((e: any) => {
				e.title = e.title.replace(/\\/g, '')
			})
			return [null, data]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Get Single Product
	async getSingle(
		productId: number
	): Promise<[errorModel | null, ProductModel | null]> {
		try {
			const response: AxiosResponse = await axios.get<ProductModel>(
				process.env.VUE_APP_API_URL + '/products/product/' + productId
			)
			const data = <ProductApiResponse>response.data
			const product: ProductModel = data.result
			if (!product) {
				throw new Error('No Product to show !')
			}
			//Transform
			product.title = product.title.replace(/\\/g, '')
			return [null, product]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Add Product
	async add(
		formData: ProductForm
	): Promise<[errorModel | null, ProductModel | null]> {
		try {
			const response: AxiosResponse = await axios.post(
				process.env.VUE_APP_API_URL + '/products/add',
				formData
			)
			const data = <ProductApiResponse>response.data
			const newProduct: ProductModel = data.result
			//Add it in the store
			store.commit('ADD_PRODUCT', newProduct)
			return [null, newProduct]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Edit Product
	async edit(
		formData: ProductForm
	): Promise<[errorModel | null, ProductModel | null]> {
		try {
			const response: AxiosResponse = await axios.put(
				process.env.VUE_APP_API_URL + '/products/' + formData.id,
				formData
			)
			const editedProduct = <ProductModel>response.data
			store.commit('EDIT_PRODUCT', editedProduct)
			return [null, editedProduct]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Delete Product
	async delete(
		productId: number
	): Promise<[errorModel | null, ProductModel | any]> {
		try {
			//Delete Image
			const [error, product] = await this.getSingle(productId)
			if (product) {
				const image = product.image
				if (image) {
					await fileApi.delete(image, 'products')
				}
			} else if (error) {
				throw new Error(error.message)
			}

			const response: AxiosResponse = await axios.delete<ProductModel>(
				process.env.VUE_APP_API_URL + '/products/' + productId
			)
			const data = <ProductModel>response.data

			//Delete from Store
			store.commit('DELETE_PRODUCT', data)

			if (data) {
				return [null, data]
			} else {
				throw new Error("Can't delete the product !")
			}
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
}
