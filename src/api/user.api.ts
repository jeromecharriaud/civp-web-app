//Axios
import axios, { AxiosResponse } from 'axios'
//Models
import { UserModel, UserForm, UserFormLogin } from '@/models/user'
import errorModel from '@/models/errorModel'
import apiResponse from '@/models/apiResponse'
import ErrorModel from '@/models/errorModel'

const route = process.env.VUE_APP_API_URL + '/users'

export default {
	//User Registration
	async add(
		credentials: UserForm
	): Promise<[ErrorModel | any, UserModel | null]> {
		try {
			const response: AxiosResponse = await axios.post<UserForm>(
				route + '/user/add',
				credentials
			)
			const data = <UserModel>response.data
			return [null, data]
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return [error.response.data, null]
			} else {
				return [error, null]
			}
		}
	},

	//Login User
	async login(
		credentials: UserFormLogin
	): Promise<[ErrorModel | any, UserModel | null]> {
		try {
			const response: AxiosResponse = await axios.post<UserFormLogin>(
				route + '/user/login',
				credentials
			)
			const data = <UserModel>response.data
			if (!data.token) {
				throw new Error('No Token !')
			}
			return [null, data]
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return [error.response.data, null]
			} else {
				return [error, null]
			}
		}
	},
	//Get Users
	async get(params?: {
		page?: number
		users?: Array<string>
	}): Promise<[ErrorModel | any, apiResponse | null]> {
		try {
			const response: AxiosResponse = await axios.get<apiResponse>(
				route,
				{
					params,
				}
			)
			const data = <apiResponse>response.data
			if (!data.result) {
				throw new Error('No users found !')
			}
			return [null, data]
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return [error.response.data, null]
			} else {
				return [error, null]
			}
		}
	},
	//Get Single User
	async getSingle(params: {
		user_id?: string
		username?: string
		email?: string
	}): Promise<[ErrorModel | any, UserModel | null]> {
		try {
			const { user_id, username, email } = params
			let path
			let variable
			if (user_id) {
				path = 'id'
				variable = user_id
			} else if (username) {
				path = 'username'
				variable = username
			} else if (email) {
				path = 'email'
				variable = email
			}
			const response: AxiosResponse = await axios.get<UserModel>(
				route + '/user/' + path + '/' + variable
			)
			const data: any = <UserModel>response.data
			if (!response.data) {
				throw new Error('No User found')
			}
			return [null, data]
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return [error.response.data, null]
			} else {
				return [error, null]
			}
		}
	},
	//Generate Token
	async getToken(
		user_id: string
	): Promise<[ErrorModel | any, UserModel | null]> {
		try {
			const response: AxiosResponse = await axios.post<UserModel>(
				route + '/user/generate-token',
				{ user_id: user_id }
			)
			const data = <UserModel>response.data
			if (!data.token) {
				throw new Error('No Token Fetched!')
			}
			return [null, data]
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return [error.response.data, null]
			} else {
				return [error, null]
			}
		}
	},
	//Activate User
	async activate(
		token: string
	): Promise<[errorModel | any, UserModel | null]> {
		try {
			const response: AxiosResponse = await axios.put(
				route + '/user/activate',
				{ token: token }
			)
			const User = <UserModel>response.data
			return [null, User]
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return [error.response.data, null]
			} else {
				return [error, null]
			}
		}
	},
	//Edit User
	async edit(
		dataForm: UserForm
	): Promise<[errorModel | any, UserModel | null]> {
		try {
			const response: AxiosResponse = await axios.put(
				route + '/user/update',
				dataForm
			)
			const editedUser = <UserModel>response.data
			return [null, editedUser]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
	//Delete User
	async delete(
		user_id: string
	): Promise<[errorModel | any, UserModel | null]> {
		try {
			const response: AxiosResponse = await axios.delete(
				route + '/user/' + user_id
			)
			const data = <UserModel>response.data
			return [null, data]
		} catch (e) {
			if (axios.isAxiosError(e) && e.response) {
				const error = <ErrorModel>e.response.data
				return [error, null]
			} else {
				const error = <ErrorModel>e
				return [error, null]
			}
		}
	},
}
