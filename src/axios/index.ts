import axios from 'axios'
export default function setup(): void {
	axios.interceptors.request.use(
		(config: any) => {
			const token = localStorage.getItem('token')

			if (token) {
				config.headers['Authorization'] = 'Bearer ' + token
			}
			return config
		},
		(error) => {
			Promise.reject(error)
		}
	)
}
