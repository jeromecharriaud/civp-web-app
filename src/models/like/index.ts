import apiResponse from '../apiResponse'
interface LikeModel {
	liked?: boolean
	user_id: string
	product_id: number
}

interface LikeApiResponse extends apiResponse {
	result: LikeModel[]
}

export { LikeModel, LikeApiResponse }
