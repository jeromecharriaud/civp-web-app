import apiResponse from '../apiResponse'

interface EventsApiResponse extends apiResponse {
	result: EventModel[]
}

interface EventApiResponse extends apiResponse {
	result: EventModel
}

interface EventModel {
	id: number
	title: string
	description: string
	created: Date
	allDay: boolean
	start: Date
	end: Date
	users: Array<string>
}

interface EventStore {
	events: EventModel[]
}

declare interface EventForm {
	id?: number
	title: string
	description: string
	created: Date
	allDay: boolean
	start: Date
	end: Date
}

export {
	EventsApiResponse,
	EventApiResponse,
	EventModel,
	EventStore,
	EventForm,
}
