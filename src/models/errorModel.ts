export default interface ErrorModel {
	code: string
	message: string
	content: any
}
