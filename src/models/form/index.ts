export default interface FormModel {
	[n: string]: string | Date | number | any
}
