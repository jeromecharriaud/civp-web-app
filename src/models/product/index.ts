import apiResponse from '../apiResponse'

interface ProductsApiResponse extends apiResponse {
	result: ProductModel[]
}

interface ProductApiResponse extends apiResponse {
	result: ProductModel
}

interface ProductModel {
	id: number
	title: string
	description: string
	image: string
	created: Date
	product_id?: number
	liked?: boolean
}

interface ProductStore {
	products: ProductModel[]
	liked: ProductModel[]
}
declare interface ProductForm {
	id?: number
	title: string | boolean
	description: string | boolean
	image: string | boolean
}

export {
	ProductsApiResponse,
	ProductApiResponse,
	ProductModel,
	ProductStore,
	ProductForm,
}
