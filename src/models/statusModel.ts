import ErrorModel from './errorModel'
export default interface StatusModel {
	error: ErrorModel
	success?: string
	warning?: string
	info?: string
}
