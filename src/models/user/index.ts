interface UserModel {
	loggedIn: boolean
	credentials: {
		id: string
		activated: boolean
		avatar: string
		name: string
		role: string
		email: string
		products_api_page: number
		products_api_index: number
		description: string
		password?: string
	}
	capabilities: {
		product: UserCapabilities
		event: UserCapabilities
		user: UserCapabilities
		like: UserCapabilities
		file: UserCapabilities
		email: UserCapabilities
	}
	token: string
}

interface UserCapabilities {
	read: boolean
	create: boolean
	update: boolean
	delete: boolean
}

interface UserForm {
	id?: string
	username?: string | boolean
	name?: string | boolean
	email?: string | boolean
	password?: string | boolean
	description?: string | boolean
	role?: string | boolean
	avatar?: string | boolean
}

declare interface UserFormLogin {
	email: string | boolean
	password: string | boolean
}
declare interface UserFormLostPassword {
	user_id?: string
	email: string
	password: string
}
declare interface UserFormPassword {
	password: string | boolean
}
declare interface UserFormProfile {
	name: string | boolean
}

declare interface UserFormAvatar {
	avatar: string | boolean
}
declare interface UserValidationForm {
	username?: boolean
	name?: boolean
	description?: boolean
	avatar?: boolean
	email?: boolean
	password?: boolean
	role?: boolean
}

export {
	UserModel,
	UserFormLostPassword,
	UserForm,
	UserFormLogin,
	UserFormPassword,
	UserFormProfile,
	UserFormAvatar,
	UserValidationForm,
}
