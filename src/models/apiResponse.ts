export default interface apiResponse {
	status: number
	offset: number
	elementsPerPage: number
	page: number
	rows: number
	total: number
	result: any
}
