export default interface Email {
	email: string | boolean
	name: string | boolean
	subject: string | boolean
	content: string | boolean
}
