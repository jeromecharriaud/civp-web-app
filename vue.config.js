module.exports = {
	lintOnSave: false,
	transpileDependencies: ['vuetify'],
	pages: {
		index: {
			entry: 'src/_app/main.ts',
			template: 'public/index.html',
			filename: 'index.html',
			title: 'App',
			chunks: ['chunk-vendors', 'chunk-common', 'index'],
		},
		admin: {
			entry: 'src/_admin/main.ts',
			template: 'public/admin.html',
			filename: 'admin.html',
			title: 'Adminstration',
			chunks: ['chunk-vendors', 'chunk-common', 'admin'],
		},
	},
	devServer: {
		historyApiFallback: {
			rewrites: [{ from: /\/admin/, to: '/admin.html' }],
		},
	},
}
